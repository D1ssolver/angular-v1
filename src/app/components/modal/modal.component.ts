import { Component, Input, OnInit } from '@angular/core';
import { ModalService } from 'src/app/services/model.service';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss']
})
export class ModalComponent implements OnInit {

  @Input() title: string

  constructor(public modelService: ModalService) { }

  ngOnInit(): void {
    
  }

}
